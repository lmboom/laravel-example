<?php

namespace App\Services;

use App\Exceptions\BussinesException;
use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CategoriesService
{
    public function index(): ?Collection
    {
        return Category::query()->get();
    }

    public function paginate($paginate = null): LengthAwarePaginator
    {
        return Category::query()->paginate($paginate);
    }

    public function store($data)
    {
        return Category::query()->create($data);
    }

    public function update(Category $category, $data): bool
    {
        return (bool)$category->update($data);
    }

    public function delete(Category $category)
    {
        if ($category->products()->exists()) {
            throw new BussinesException('У категории имеются продукты',403);
        }
        try {
            $category->delete();
        } catch (\Exception $e) {
            throw new BussinesException($e->getMessage(),$e->getCode());
        }
    }


}
