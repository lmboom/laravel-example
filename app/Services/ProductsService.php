<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Collection;
use App\Http\Filters\ProductsFilter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProductsService
{
    public function index(): ?Collection
    {
        return Product::all();
    }

    public function paginate(ProductsFilter $filters, $paginate = null): LengthAwarePaginator
    {
        return Product::query()->filterBy($filters)->paginate($paginate);
    }

    public function update(Product $product, $data): bool
    {
        return (bool)$product->update($data);
    }

    public function store($data): ?Product
    {
        $product = Product::create($data);

        if(isset($data['categories_id'])) {
            $product->categories()->attach($data['categories_id']);
        }

        return $product;
    }

    public function delete(Product $product)
    {
        $product->categories()->detach();
        $product->delete();
    }
}
