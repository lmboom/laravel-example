<?php

namespace App\Http\Filters;

use App\Http\Filters\Core\QueryFilters;
use Illuminate\Database\Eloquent\Builder;


class ProductsFilter extends QueryFilters
{
    public function title($search_text)
    {
        $this->query->where('title','LIKE', "%{$search_text}%");
    }

    public function categoryId($id)
    {
        $this->query->whereHas('categories', function (Builder $categories) use ($id){
            $categories->where('id', $id);
        });
    }

    public function categoryName($category)
    {
        $this->query->whereHas('categories', function (Builder $categories) use ($category){
            $categories->where('name','LIKE', "%{$category}%");
        });
    }

    public function priceFrom($from)
    {
        $this->query->where('price', '>=', $from);
    }

    public function priceTo($to)
    {
        $this->query->where('price', '<=', $to);
    }

    public function isPublished($bool)
    {
        $this->query->where('is_published', $bool);
    }

    public function active($bool)
    {
        if ($bool == true) {
            $this->query->whereNull('deleted_at');
        }
    }
}
