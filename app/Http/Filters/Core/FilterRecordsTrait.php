<?php

namespace App\Http\Filters\Core;

use Illuminate\Database\Eloquent\Builder;

trait FilterRecordsTrait
{
    /**
     * Filter records based on the given query filters
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param QueryFilters $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterBy(Builder $query, QueryFilters $filters)
    {
        return $filters->applyToQuery($query);
    }
}
