<?php

namespace App\Http\Requests;

class ProductRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:categories',
            'categories_id' => 'nullable|array',
            'price' => 'required|numeric|min:0',
            'categories_id.*' => 'required_with:categories_id|exists:categories,id',
            'is_published' => 'required|boolean'
        ];
    }
}
