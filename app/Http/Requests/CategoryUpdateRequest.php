<?php

namespace App\Http\Requests;

class CategoryUpdateRequest extends CategoryRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'name' => 'required|unique:categories,name,' . $this->route('category')->id
        ]);
    }
}
