<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'title' => $this->title,
          'price' => $this->price,
          'categories' => CategoriesResource::collection($this->whenLoaded('categories')),
          'is_published' => $this->is_published,
          'created_at' => $this->created_at
        ];
    }
}
