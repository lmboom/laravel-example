<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductsResource;
use App\Models\Product;
use App\Services\ProductsService;
use App\Http\Filters\ProductsFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $service;

    public function __construct(ProductsService $service)
    {
        $this->service = $service;
    }

    public function index(ProductsFilter $filters)
    {
        $products = $this->service->paginate($filters);

        return ProductsResource::collection($products);
    }

    public function update(Product $product, Request $request)
    {
        $this->service->update($product, $request->all());

        return new ProductsResource($product);
    }

    public function store(ProductRequest $request)
    {
        $product = $this->service->store($request->validated());

        return new ProductsResource($product);
    }

    public function destroy(Product $product)
    {
        $this->service->delete($product);

        return response()->json(['message' => 'Продукт успешно удален!']);
    }
}
