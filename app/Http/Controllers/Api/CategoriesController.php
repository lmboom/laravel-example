<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\BussinesException;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoriesResource;
use App\Models\Category;
use App\Services\CategoriesService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;

class CategoriesController extends Controller
{
    private $service;

    public function __construct(CategoriesService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $categories = $this->service->paginate();

        return CategoriesResource::collection($categories);
    }

    public function store(CategoryRequest $request)
    {
        $category = $this->service->store($request->validated());

        return new CategoriesResource($category);
    }

    public function update(Category $category, CategoryUpdateRequest $request)
    {
        $this->service->update($category, $request->validated());
    }

    public function destroy(Category $category)
    {
        $this->service->delete($category);

        return response()->json(['message' => 'Категория успешно удалена!']);
    }
}
