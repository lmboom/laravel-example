<?php

namespace App\Exceptions;

use Exception;

class BussinesException extends Exception
{
    public function render()
    {
        return response()->json(['message' => $this->getMessage()], $this->getCode());
    }
}
