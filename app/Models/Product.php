<?php

namespace App\Models;

use App\Http\Filters\Core\FilterRecordsTrait;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, FilterRecordsTrait;

    protected $fillable = ['title','price','is_published'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

//relations
    public function categories()
    {
        return $this->belongsToMany(Category::class,'product_category');
    }
}
