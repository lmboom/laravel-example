<?php

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create categories

        $categories = factory(Category::class, 20)->create();

        factory(Product::class, 100)
            ->create()
            ->each(function (Product $product) use ($categories) {
                $product->categories()->attach($categories->shuffle()->first()->id);
            });
    }
}
